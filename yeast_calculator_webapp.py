# -*- coding: utf-8 -*-
import dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output
import math


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div([
    dcc.Tabs([
        dcc.Tab(label='Starting Concentration', children=[
                html.H6("Calculate Starting Concentration"),

                html.Div(["Final Concentration (cells/mL): ",
                        dcc.Input(id='fc-input-t1', value='500000', type='text')]),
                html.Div(["Doubling Time (min): ",
                        dcc.Input(id='dt-input-t1', value='70', type='text')]),
                html.Div(["Duration (min): ",
                        dcc.Input(id='dur-input-t1', value='480', type='text')]),
 
                html.Br(),
                html.Div(id='sc-output'),
                html.Br(),

                html.H6("Dilute to Calculated Starting Concentration"),
                html.Div(["Current Concentration (cells/mL): ",
                        dcc.Input(id='cc-input-t1', value='500000', type='text')]),
                html.Div(["Total Volume (mL): ",
                        dcc.Input(id='tv-input-t1', value='25', type='text')]),
                html.Div(id='dil-output-t1'),
                html.Br(),

        ]),
        dcc.Tab(label='Final Concentration', children=[
                html.H6("Calculate Final Concentration"),
                html.Div(["Starting Concentration (cells/mL): ",
                        dcc.Input(id='fc-input-t2', value='1000', type='text')]),
                html.Div(["Doubling Time (min): ",
                        dcc.Input(id='dt-input-t2', value='70', type='text')]),
                html.Div(["Duration (min): ",
                        dcc.Input(id='dur-input-t2', value='480', type='text')]),
 
                html.Br(),
                html.Div(id='fc-output'),
                html.Br(),
        ]),
        dcc.Tab(label='Duration', children=[
                html.H6("Calculate Duration"),
                html.Div(["Starting Concentration (cells/mL): ",
                        dcc.Input(id='sc-input-t3', value='1000', type='text')]),
                html.Div(["Final Concentration (cells/mL): ",
                        dcc.Input(id='fc-input-t3', value='500000', type='text')]),
                html.Div(["Doubling Time (min): ",
                        dcc.Input(id='dt-input-t3', value='70', type='text')]),

                html.Br(),
                html.Div(id='dur-output'),
                html.Br(),

        ]),
        dcc.Tab(label='Doubling Time', children=[
                html.H6("Calculate Doubling Time"),
                html.Div(["Starting Concentration (cells/mL): ",
                        dcc.Input(id='sc-input-t4', value='10000', type='text')]),
                html.Div(["Final Concentration (cells/mL): ",
                        dcc.Input(id='fc-input-t4', value='500000', type='text')]),
                html.Div(["Duration (min): ",
                        dcc.Input(id='dur-input-t4', value='480', type='text')]),
 
                html.Br(),
                html.Div(id='dt-output'),
                html.Br(),

        ]),
        dcc.Tab(label='Dilution', children=[
                html.H6("Calculate Dilution"),
                html.Div(["Desired Concentration (cells/mL): ",
                        dcc.Input(id='dc-input-t5', value='100000', type='text')]),
                html.Div(["Current Concentration (cells/mL): ",
                        dcc.Input(id='cc-input-t5', value='500000', type='text')]),
                html.Div(["Total Volume (mL) ",
                        dcc.Input(id='tv-input-t5', value='25', type='text')]),

                html.Br(),
                html.Div(id='dil-output-t5'),
                html.Br(),

        ]),
    ])
])


@app.callback(
    Output(component_id='sc-output', component_property='children'),
    Input(component_id='dt-input-t1', component_property='value'), 
    Input(component_id='fc-input-t1', component_property='value'), 
    Input(component_id='dur-input-t1', component_property='value')
)
def update_starting_concentration_output(input_value1, input_value2, input_value3):
    return 'Starting concentration: ', round(float(input_value2)/(2**(float(input_value3)/float(input_value1))),0), " cells/mL"


@app.callback(
    Output(component_id='dil-output-t1', component_property='children'),
    Input(component_id='cc-input-t1', component_property='value'), 
    Input(component_id='tv-input-t1', component_property='value'), 
    Input(component_id='sc-output', component_property='children')
)
def update_sc_dilution_output(input_value1, input_value2, input_value3):
    return 'Dilution: ', round(float(input_value3[1])*float(input_value2)/float(input_value1), 4), ' mL of current culture into specified final volume'



@app.callback(
    Output(component_id='fc-output', component_property='children'),
    Input(component_id='dt-input-t2', component_property='value'), 
    Input(component_id='fc-input-t2', component_property='value'), 
    Input(component_id='dur-input-t2', component_property='value')
)
def update_final_concentration_output(input_value1, input_value2, input_value3):
    return 'Final Concentration: {} cells/mL'.format(round(float(input_value2)*(2**(float(input_value3)/float(input_value1))),2))

@app.callback(
    Output(component_id='dur-output', component_property='children'),
    Input(component_id='dt-input-t3', component_property='value'), 
    Input(component_id='fc-input-t3', component_property='value'), 
    Input(component_id='sc-input-t3', component_property='value')
)
def update_duration_output(input_value1, input_value2, input_value3):
    return 'Duration: {} min'.format(int(float(input_value1)*math.log(float(input_value2)/float(input_value3))/math.log(2)))

@app.callback(
    Output(component_id='dt-output', component_property='children'),
    Input(component_id='dur-input-t4', component_property='value'), 
    Input(component_id='fc-input-t4', component_property='value'), 
    Input(component_id='sc-input-t4', component_property='value')
)
def update_doubling_time_output(input_value1, input_value2, input_value3):
    return 'Doubling Time: {} min'.format(int(float(input_value1)*math.log(2)/math.log(float(input_value2)/float(input_value3))))

@app.callback(
    Output(component_id='dil-output-t5', component_property='children'),
    Input(component_id='cc-input-t5', component_property='value'), 
    Input(component_id='tv-input-t5', component_property='value'), 
    Input(component_id='dc-input-t5', component_property='value')
)
def update_sc_dilution_output(input_value1, input_value2, input_value3):
    return 'Dilution: ', round(float(input_value3)*float(input_value2)/float(input_value1), 4), ' mL of current culture into specified final volume'



if __name__ == '__main__':
    app.run_server(debug=True)