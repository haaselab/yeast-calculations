import sys
import argparse
import math

calc = True
while calc == True:
    calculation = input("What would you like to calculate? Start concentration, final concentration, duration, dilution, or doubling time? ")
    if calculation not in ['final concentration', 'start concentration', 'doubling time', 'duration', 'dilution']:
        calc = True
        print("\nPlease type one of the options: start concentration, final concentration, doubling time, dilution or duration")
    else:
        calc = False

assert calculation in ['final concentration', 'start concentration', 'doubling time', 'dilution', 'duration',], \
        'Calculation is not one of final concentration, start concentration, doubling time, dilution, or duration: bailing out'
if calculation == "final concentration":
    test = False
    while test == False:
        try:
            starting_conc = float(input("What is your starting concentration (cells/mL)? " ))
            assert starting_conc >=0
            doubling_time =  float(input("What is the doubling time (in minutes)? "))
            assert doubling_time >=0
            duration =  input("What is the duration the culture will be left? Please enter in the following format: hours minutes (eg. for 5 hours and 10 minutes '5 10') " )
            duration = duration.split()
            duration = float(duration[0])*60 + float(duration[1])
            assert duration >=0
            value = starting_conc * 2**(duration/doubling_time)
            units = " cells/mL"
            test = True
        except:
            print("Please enter a valid number for all inputs. Make sure you have followed the required input format and have no included units: eg 1000 and not 1000 mL")
            test = False
            
elif calculation == "doubling time":
    test = False
    while test == False:
        try:
            starting_conc =  float(input("What is your starting concentration (cells/mL)?" ))
            assert starting_conc >=0
            final_conc =  float(input("What is the final concenetraion (in cells/mL)?" ))
            assert final_conc >=0
            duration =  input("What is the duration the culture will be left? Please enter in the following format: hours minutes (eg. for 5 hours and 10 minutes '5 10') " )
            duration = duration.split()
            duration = float(duration[0])*60 + float(duration[1])
            assert duration >=0
            value = duration * math.log(2)/(math.log(final_conc/starting_conc))
            units = " min"
            if starting_conc < final_conc:
                test = True
            elif starting_conc >= final_conc:
                print("Please make start concentration smaller than final concentration to calculate doubling time")
                test = False
        except:
            print("Please enter a valid number (with no units) for all inputs, for example 100000 and not 10000 cells/mL, and check that starting concentration doesn't equal final concentration")
            test = False
        
elif calculation == "duration":
    test = False
    while test == False:
        try:
            starting_conc =  float(input("What is your starting concentration (cells/mL)?" ))
            assert starting_conc >=0
            final_conc =  float(input("What is the final concenetraion (in cells/mL)?" ))
            assert final_conc >=0
            doubling_time =  float(input("What is the doubling time (in minutes)?" ))
            assert doubling_time >=0
            value = doubling_time * math.log(final_conc/starting_conc)/math.log(2)
            hours = round(value / 60)
            minutes = round(value % 60)
            value = str(str(hours)+ " hours " + str(minutes)) 
            units = " minutes "
            test = True
        except:
            print("Please enter a valid number (with no units) for all inputs, for example 100000 and not 10000 cells/mL")
            test = False
            
elif calculation == "start concentration":
    test = False
    while test == False:
        try:
            final_conc =  float(input("What is the final concenetraion (in cells/mL)?" )) 
            assert final_conc >= 0
            doubling_time =  float(input("What is the doubling time (in minutes)?" ))
            assert doubling_time >=0
            duration =  input("What is the duration the culture will be left? Please enter in the following format: hours minutes (eg. for 5 hours and 10 minutes '5 10') " )
            duration = duration.split()
            duration = float(duration[0])*60 + float(duration[1])
            assert duration >=0
            value = final_conc/(2**(duration/doubling_time))
            start_conc = value
            units = " cells/mL"
            test = True
        except:
            print("Please enter a valid number (with no units) for all inputs, for example 100000 and not 10000 cells/mL")
            test = False
            
elif calculation == "dilution":
    test = False
    while test == False:
        try:
            starting_conc = input("Is start concentration known? If starting concentration is known, please enter in cells/mL. If starting concentration needs to be calculated please enter 'n'")
            if starting_conc == "n":
                final_conc =  float(input("What is the final concenetraion (in cells/mL)?" )) 
                assert final_conc >= 0
                doubling_time =  float(input("What is the doubling time (in minutes)?" ))
                assert doubling_time >=0
                duration =  input("What is the duration the culture will be left? Please enter in the following format: hours minutes (eg. for 5 hours and 10 minutes '5 10') " )
                duration = duration.split()
                duration = float(duration[0])*60 + float(duration[1])
                assert duration >=0
                current_conc = float(input("What is the current concentration?"))
                assert current_conc >=0
                final_vol = float(input("What would you like the final volume to be (in mL)"))
                assert final_vol >=0
                start_conc = final_conc/(2**(duration/doubling_time))
                value = final_vol * start_conc/current_conc
                test = True
            else:
                starting_conc = float(starting_conc)
                assert starting_conc >= 0
                final_conc =  float(input("What is the final concenetraion (in cells/mL)?" )) 
                assert final_conc >= 0
                final_vol = float(input("What would you like the final volume to be (in mL)"))
                assert final_vol >=0
                value = final_vol * final_conc / starting_conc
                if starting_conc < final_conc:
                    print("Please make starting concentration larger than final concentration to calculate a dilution." )
                    test = False
                elif starting_conc>= final_conc:
                    test = True
            units = " mL in final volume"
           
        except:
            print("Please enter a valid number (with no units) for all inputs, for example 100000 and not 10000 cells/mL")
            test = False           
    

print (str(calculation) + " = " + str(value) + str(units)) 
    
    
if calculation == "duration" and starting_conc>final_conc:

    test = False
    while test == False:
        try:
            dil_yn = input("Are you sure you want to calculate duration? Your starting concentration is greater than your final concentration. Do you want to calculate the dilution? (y/n)")
            if dil_yn == "y":
                final_vol = float(input("What would you like the final volume to be (in mL)?"))
                assert final_vol >=0
                value_new = final_vol * final_conc/starting_conc
                print("volume from start culture = " +str(value_new) + " mL of start in " + str(final_vol) + "mL final volume")
                test = True
            elif dil_yn == "n":
                test = True
            else:
                test = False
        except:
            print("please enter a valid number with no units (eg. 10000 not 10000 mL) for all inputs")
            
   


if calculation == "start concentration":
    test = False
    while test == False:
        try:
            dil_yn = input("If your current concentration is greater than the calculated start concentration, do you want to calculate the dilution? (y/n)")
            if dil_yn == "y":
                current_conc = float(input("What is the current concentration?"))
                assert current_conc >=0
                final_vol = float(input("What would you like the final volume to be (in mL)"))
                assert final_vol >=0
                value_2 = final_vol * start_conc/current_conc
                print("volume from current culture = " +str(value_2) + " mL in " + str(final_vol) + "mL final volume")
                test = True
            elif dil_yn == "n":
                print (str(calculation) + " = " + str(value) + str(units)) 
                test = True
            else:
                test = False
        except:
            print("Please enter a valid number (with no units), for example 100000 and not 10000 cells/mL for all inputs")
            test = False
  
    
    
#   functionize it?






    
    
    
    

