Using Previous Versions of the Yeast Calculator:

# Yeast Calculator GUI
## Requirements:
Python, tkinter, math

## Using the Calculator
Running the program will cause a tkinter window to appear. The drop down menu provides the options for types of calculations: including starting concentration, final concentration, duration of growth, doubling time and dilution.

The calculation of interest can be selected and the inputs can be entered according to the descriptions and units provided. The calculation will be performed and the result displayed after the 'OK' button is clicked.

For the starting concentration calculation after clicking the 'OK' button, a dilution calculator will appear. If the current concentration of the culture is higher than that of the desired starting concentration, the current culture's concentration and the desired final volume can be entered to calculate the dilution to the calculated starting concentration.

Mac OS update 10.14.6 causes tkinter to crash. An alternative version that requests inputs in the command line has been added.


# Interactive Yeast Calculator (python notebooks)
## Requirements:
Python, jupyter notebook or jupyter lab, ipywidgets, math

## Using the Calculator
To run the calculator, open a jupyter notebook or a jupyter lab notebook. Run the
Interactive calculator code using the command "%run Interactive_calculator.py" using the correct path. Then run the command "VBox(children = [tab])". The calculator will appear. Select the tab with the desired calculation and input the requested information. Defaults will appear in the text boxes. The result will be calculated and displayed at the bottom of the calculator. The result will update as numbers in the boxes are altered.


## Author
**Sophie Campione**
Haase Lab, 2020
