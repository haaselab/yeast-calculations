from ipywidgets import interact, FloatSlider, interact_manual
import ipywidgets as widgets
import math
from ipywidgets import Button, HBox, VBox, Label


fcbox = widgets.BoundedFloatText(min = 0.0, step = 0.01, max = 10000000000, value = 10000000)
scbox =widgets.BoundedFloatText(min = 0.0, step = 0.01, max = 10000000000, value = 1000000)
dtbox = widgets.BoundedFloatText(min = 0.0, step = 0.1, max = 10000000000, value =70)
durbox = widgets.BoundedFloatText(min = 0.0, step = 0.01, max = 10000000000, value = 6)
volbox = widgets.BoundedFloatText(min = 0.0, step = 0.0001, max = 10000000000, value = 5.0)
def fsc(a, b, c):
    print(a/(2 **(c/(b/60))))
outsc = widgets.interactive_output(fsc, {'a': fcbox, 'b':dtbox, 'c':durbox})
def ffc(a, b, c):
    print(a*(2 **(c/(b/60))))
outfc = widgets.interactive_output(ffc, {'a': scbox, 'b':dtbox, 'c':durbox})
def fdt (a, b, c):
    print(c*60*math.log(2)/math.log(b/a))
outdt = widgets.interactive_output(fdt, {'a':scbox, 'b':fcbox, 'c':durbox})
def fdur (a,b,c):
    print(c/60*math.log(b/a)/math.log(2))
outdur = widgets.interactive_output(fdur, {'a':scbox, 'b':fcbox, 'c':dtbox})
def fdil(a,b,c):
    print(a*c/b)
outdil = widgets.interactive_output(fdil, {'a':scbox,'b':fcbox, 'c':volbox})

calculations = ["start concentration", "final concentration", "doubling time", "duration", "dilution"]
sc = VBox(children=[widgets.Label("final concentration (cells/mL):"), fcbox,widgets.Label("doubling time (min)"), dtbox,widgets.Label("duration (hours):"), durbox, widgets.Label("Calculated Starting Concentration:"), outsc] )
fc = VBox(children=[widgets.Label("start concentration (cells/mL):"), scbox,widgets.Label("doubling time (min)"), dtbox,widgets.Label("duration (hours):"),durbox,widgets.Label("Calculated Final Concentration:"), outfc] )
dt = VBox(children=[widgets.Label("start concentration (cells/mL):"),scbox,widgets.Label("final concentration (cells/mL)"), fcbox,widgets.Label("duration (hours):"), durbox, widgets.Label("Calculated Doubling Time:"), outdt] )
dur = VBox(children=[widgets.Label("start concentration (cells/mL):"), scbox,widgets.Label("final concentration (cells/mL)"), fcbox,widgets.Label("doubling time (min):"), dtbox, widgets.Label("Calculated Duration (hours):"), outdur] )
dil = VBox(children = [widgets.Label("current concentration (cells/mL):"), scbox, widgets.Label("desired concentration (cells/mL):"), fcbox, widgets.Label("final volume (mL):"),volbox, widgets.Label("Calculated Volume of Sample (mL):"), outdil ])

tabs =[sc, fc, dt, dur, dil]
tab = widgets.Tab(children=tabs)
for i in range(0,len(calculations)):
    tab.set_title(i, calculations[i])

VBox(children=[tab])