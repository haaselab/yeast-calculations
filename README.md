# Yeast Calculator Web App
==============================

A simple cell count calculator. Able to calculate starting concentration, final concentration, duration of growth, doubling time and dilutions.

## Requirements:
python, dash, dash_html_components, dash_core_components, math

## Calculations

Calculations for starting concentration, final concentration, duration and doubling time are derived from the equation:
final_concentration = starting_concentration * 2 ^(duration / doubling_time)

Calculations for dilutions were derived using the equation:
C1 * v1 = C2 * v2

# Using the Yeast Calculator App


Run Yeast Calculator App
------------
```
In /yeast-calculations , run:
    $ python yeast_calculator_webapp.py

Click the link that follows "Dash is running on "
```
Select the tab with the desired calculation and input the requested information. Defaults will appear in the text boxes.
The result will be calculated and displayed at the bottom of the calculator. The result will update as numbers in the boxes are altered.


## Author
**Sophie Campione**
Haase Lab, 2021
